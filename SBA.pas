program football;
uses WinCrt, Crt;

const MaxTeam = 100;
      MaxRule = 20;
      MaxValidate = MaxRule * 2;

type TeamRec = record
                    Name, District : string;
                    Ranking : integer;
                    Seed, Valid : boolean;
                    Rule : array [1..MaxRule] of string;
                 end;

var TeamNum : integer;
    Team : array [0..MaxTeam] of TeamRec;
    //           (Team Number)  ; Team[0] for index
    Validate : array [1..MaxValidate, 0..MaxTeam] of string;
    //               (Rule Number)    (Team Number)
    ValidateNum : array [1..MaxValidate] of integer;
    //                  (Rule Number)
    Sequence : array [1..MaxRule+5] of integer;
    SpecialSeq : array [1..MaxRule+5] of char;
    Infile, Valifile : text;

function CheckValid(item : string; v, top, bottom : integer) : integer;
var middle : integer;
begin
   middle := (top + bottom) div 2;
   if top > bottom then
      CheckValid := 0
   else if item = Validate[v, middle] then
         CheckValid := middle
      else if item > Validate[v, middle] 
         then CheckValid := CheckValid(item, v, middle + 1, bottom)
         else CheckValid := CheckValid(item, v, top, middle - 1) 
end;

function str2int(s : string) : integer;
var int, error : integer;
begin
   val(s, int, error);
   if (int > 0) and (error = 0)
      then str2int := int
      else str2int := 0
end;

function str2boo(s : string) : boolean;
begin
   if (s = 'YES') or (s = 'Yes') or (s = 'yes') or (s = 'Y') or (s = 'y')
         then str2boo := true
         else str2boo := false
end;

procedure Sort(item : integer);
var i, j : integer;
    temp : string;
begin
   for i := 2 to ValidateNum[item] do begin
      temp := Validate[item, i];
      j := i;
      while (j > 1) and (Validate[item, j-1] > temp) do begin
         Validate[item, j] := Validate[item, j-1];
         j := j - 1;
      end;
      Validate[item, j] := temp
   end;
end;

procedure HIT; //Test Flight
var i : integer;
begin
   for i := 1 to MaxRule + 5 do begin
      if Sequence[i] = 0 then
         write(SpecialSeq[i], ' ')
      else
         write(Sequence[i], ' '); 
   end;
   writeln;
   writeln('Press any button to continue...');
   readkey;
   ClrScr
end; 

procedure TeamInputNew; //Too Cool for School
var tempS, s, t : String;
    temp : char;
    i, j, k, nameL : integer;
begin
   //Input record
   i := 0;
   while not EOF(Infile) do begin
      i := i + 1; j := 1;
      repeat
         //Input entry
         tempS := '';
         nameL := 0;
         repeat
            read(Infile, temp);
            if not (((tempS = '') and (temp = ' ')) or ((tempS[nameL] = ' ') and (temp = ' ')))  then
               tempS := tempS + temp;
            nameL := length(tempS);
         until (tempS[nameL] = '|') or EOln(Infile);
         tempS := copy(tempS, 1, nameL - 2);
         case SpecialSeq[j] of
            'N' : Team[i].Name := tempS;
            'D' : Team[i].District := tempS;
            'R' : Team[i].Ranking := str2int(tempS);
            'S' : Team[i].Seed := str2boo(tempS);
         end;
         Team[i].Valid := true;
         j := j + 1
      until EOln(Infile);
      readln(Infile);
      writeln('Entry ', i, ' imported.')
   end;
   TeamNum := i;
end;

procedure TeamInput; //obsolete
var tempName, s : String;
    temp : char;
    i, j, k, nameL : integer;
begin
   i := 0;
   while not EOF(Infile) do begin
      i := i + 1;
      //Inputting name
      tempName := '';
      nameL := 0;
      repeat
         read(Infile, temp);
         if not (((tempName = '') and (temp = ' ')) or ((tempName[nameL] = ' ') and (temp = ' ')))  then
            tempName := tempName + temp;
         nameL := length(tempName);
      until (tempName[nameL] = '|');
      Team[i].Name := copy(tempName, 1, nameL - 2);
      //Inputting district
      tempName := '';
      nameL := 0;
      repeat
         read(Infile, temp);
         if not ((tempName = '') and (temp = ' ')) then
            tempName := tempName + temp;
         nameL := length(tempName);
      until (tempName[nameL] = '|');
      Team[i].District := copy(tempName, 1, nameL - 2);
      //Inputting Ranking
      read(Infile, j);
      Team[i].Ranking := j;
      //Inputting seeding
      read(Infile, s);
      s := copy(s, 4, length(s));
      if (s = 'YES') or (s = 'Yes') or (s = 'yes') or (s = 'Y') or (s = 'y')
         then Team[i].Seed := true
         else Team[i].Seed := false;
      Team[i].Valid := true;
      readln(Infile); //EOln
   end;
   TeamNum := i
end;

procedure HeaderInput; //Too Cool for School
var tempS, nameS, t : String;
    temp : char;
    i, j, k, nameL : integer;
begin
   //Input header
   i := 0;
   j := 0; 
   repeat
      i := i + 1;
      //Input header entry
      tempS := '';
      nameS := '';
      repeat
         read(Infile, temp);
         nameL := length(tempS);
         if not (((tempS = '') and (temp = ' ')) or ((tempS[nameL] = ' ') and (temp = ' ')))  then
            tempS := tempS + temp;
      until (tempS[nameL] = '|') or EOln(Infile);
      nameS := tempS;
      if (tempS[nameL] = '|') then
         nameS := copy(nameS, 1, nameL - 1);
      nameL := length(nameS);
      if (tempS[nameL] = ' ') then
         nameS := copy(nameS, 1, nameL - 1);
      //Hotword Search
      if nameS = 'Name' then SpecialSeq[i] := 'N' else
      if nameS = 'District' then SpecialSeq[i] := 'D' else
      if nameS = 'Ranking' then SpecialSeq[i] := 'R' else
      if nameS = 'Seed' then SpecialSeq[i] := 'S' else
      begin
         j := j + 1;  
         Team[0].Rule[j] := nameS;
         SpecialSeq[i] := ' '
      end;      
      if (nameS = 'Name') or (nameS = 'District') or (nameS = 'Ranking') or (nameS = 'Seed')
         then Sequence[i] := 0
         else Sequence[i] := j;
   until EOln(Infile);
   readln(Infile);
   writeln('Header Imported.');
   //HIT //(Test ended)
   TeamInputNew //(Testing)
   //TeamInput
end;

procedure Validator; //(item : integer);
var i, j, k : integer;
begin
   //if item = '0' then "check all"
   i := 0;
   j := 0;
   repeat
      i := i + 1;
      repeat
         j := j + 1;
         if Validate[i, 0] = 'District'{Team[0].Rule[j]} then
         //if (Validate[i, 0] = Team[0].Rule[j]) or (item = 0) then
            for k := 1 to TeamNum do
               if (CheckValid(Team[k].District{Rule[j]}, i, 1, ValidateNum[i]) = 0) then begin
                  Team[k].Valid := false;
                  writeln(Team[k].Name, ' is not a valid team.')
               end;
      until (Validate[i, 0] = 'District'{Team[0].Rule[j]}) or (j >= MaxRule)        
   until (Validate[i, 0] = 'District'{Team[0].Rule[j]}) or (i >= MaxValidate)
end;   

procedure ValidateInput;
var temp : String;
    i, j, k, nameL : integer;
begin
   i := 0;
   j := 0;
   readln(Valifile, temp);
   repeat
      j := j + 1;
      if Validate[j,0] = '' then
         Validate[j,0] := temp
   until (j >= MaxValidate) or (Validate[j,0] = temp);
   repeat
      i := i + 1;
      //Inputting items
      readln(Valifile, temp);
      Validate[j,i] := temp;
   until EOF(Valifile);
   ValidateNum[j] := i;
   Sort(j);
   if teamNum > 0 then Validator;
   //if teamNum > 0 then Validator(j);
end;

procedure CheckFile;
var Filename : string;
begin
   write('Please input the filename: ');
   readln(Filename);
   assign(Infile, Filename);
   {$i-}
   reset(Infile);
   {$i+}
   Filename := '';
   if (IOResult <> 0) then begin
      writeln('This file does not exist. Please try again.');
      CheckFile;
   end;
   //TeamInput
   HeaderInput
end;

procedure CheckValiFile;
var Filename : string;
begin
   write('Please input the filename: ');
   readln(Filename);
   assign(Valifile, Filename);
   {$i-}
   reset(Valifile);
   {$i+}
   Filename := '';
   if (IOResult <> 0) then begin
      writeln('This file does not exist. Please try again.');
      CheckValiFile;
   end;
   ValidateInput
end;

procedure PrintInput;
var i : integer;
begin
   for i := 1 to TeamNum do begin
      with Team[i] do begin
         write(Name:35);
         write(District:20);
         write(Ranking:3);
         write(Seed:6);
         writeln(Valid:6);
      end;
   end;
   writeln('Press any button to continue...');
   readkey;
   ClrScr
end;

procedure PrintValidate;
var i, j : integer;
begin
   for i := 1 to MaxValidate do
      if Validate[i,0] <> '' then begin
         writeln('Valid item for rule "', Validate[i,0], '" :');
         for j := 1 to ValidateNum[i] do
            writeln(Validate[i,j])
      end
end;

procedure Menu;
var option : char;
begin
   repeat
      repeat
         writeln('Please choose an option from: ');
         writeln('1: Import a new team list');
         writeln('2: Import validation File');
         writeln('3: Print imported team list');
         writeln('4: Print validation items');
         writeln('5: Validate'); //temp
         writeln('Q: Quit application');
         write('Option: ');
         readln(option);
         if ((option >= 'a') and (option <= 'z')) then
            option := chr(ord(option) - 32);
      until option in ['1', '2', '3', '4', '5', '6', 'Q'];
      case option of
         '1' : begin
                  CheckFile;
                  writeln('New team list imported.');
                  writeln('Press any button to continue...');
                  readkey;
                  ClrScr
               end;
         '2' : begin
                  CheckValiFile;
                  writeln('Validation file imported.');
                  writeln('Press any button to continue...');
                  readkey;
                  ClrScr
               end;
         '3' : PrintInput;
         '4' : PrintValidate;
         '5' : Validator; //temp
         'Q' : begin
                  writeln('Press any button to quit...');
                  readkey
               end
      end;
   until option = 'Q'
end;


begin
   Menu;
   // TeamInputNew;
   // RuleInput;
   readkey
end.
